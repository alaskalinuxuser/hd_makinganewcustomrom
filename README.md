# HD Making a new type of custom rom

# About
I get a lot of questions about how to compile Android and how to make custom kernels, so I thought I would put together a video series with everything I know. (Don't worry, that will not take long!)
Here we will be making a new type of custom rom, like lineageOS/RR/Slim etc., but we will call ours "UselessRom".

# Who is this course for?
This course is for those who are able to build your very own custom roms and kernels, and now want to make your own custom rom.

